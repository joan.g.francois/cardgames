const colors = ["hearts", "diamonds", "clubs", "spades"];

const initCards = () => {
    var cards =[];
    for(var i in colors){
        for(var j=2; j<=14; j++){
            cards.push({color: colors[i], value: j});
        }
    }
    cards.sort(() => Math.random() - 0.5);
    return  cards;
};


module.exports.pickaxeToDeck = (gameInfo) => {
	var card = gameInfo.body.pickaxe.pop();
	gameInfo.body.deck= gameInfo.body.pickaxe;
	gameInfo.body.deck.sort(() => Math.random() - 0.5);
	gameInfo.body.pickaxe = [];
	gameInfo.body.pickaxe.push(card);
	return gameInfo;
};

module.exports.initGameData = () => {
	return { deck: initCards(),
	         pickaxe: []
		   }
};

