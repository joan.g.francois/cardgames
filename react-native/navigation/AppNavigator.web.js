import { createBrowserApp } from '@react-navigation/web';
import { createStackNavigator } from 'react-navigation';


import HomeScreen from '../screens/HomeScreen';
import GameScreen from '../screens/GameScreen';

//import MainTabNavigator from './MainTabNavigator';
const MainNavigator = createStackNavigator({
  Home: {screen: HomeScreen},
  Game: {screen: GameScreen}
});


export default createBrowserApp(MainNavigator, { history: 'hash' });
