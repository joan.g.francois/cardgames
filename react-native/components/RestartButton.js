import React from 'react';
import { TouchableHighlight} from 'react-native';
import { Icon } from 'react-native-elements';

export default function RestartButton(props){
    return (<TouchableHighlight 
                style={props.style}
                onPress={()=>{
                    props.websocket.send(JSON.stringify({
                        action:"restartgame",
                        data: JSON.stringify({gameId: props.gameId})
                    }));
                }}>
                <Icon name='autorenew'
                      color='#00aced'/>
            </TouchableHighlight>);
}

