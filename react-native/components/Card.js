import React from 'react';
import { View, StyleSheet, Text, PanResponder, Animated } from 'react-native';
import CardStyles from "./CardStyles"
import { getSymbole} from "../helpers/CardsHelper"
import Emoji from 'react-native-emoji';

export default class Card extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      pan: new Animated.ValueXY(),
      isHide: props.isHide,
    };
  }

  componentWillMount() {    // Add a listener for the delta value change
    this._val = { x:0, y:0 }
    this.state.pan.addListener((value) =>{ 
         this._val = value
    });    // Initialize PanResponder with move handling
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (e, gesture) => true,
      onPanResponderMove: Animated.event([
            null, { dx: this.state.pan.x, dy: this.state.pan.y, zIndex: 200 }
      ]),
      onPanResponderGrant: (e, gesture) => {
          this.state.pan.setOffset({
            x: this._val.x,
            y:this._val.y
          })
          this.state.pan.setValue({ x:0, y:0})
      },
      onPanResponderRelease: (e, gesture) => {  
        if (this.isDropArea(gesture) || this.isPickaxeArea(gesture)) {
            
            var data= {cardColor: this.props.color, 
                       cardValue: this.props.value,
                       index: this.props.index,
                       status: this.props.status};
            if(this.isDropArea(gesture) && this.props.status!=="playerHand"){
                data.action="draw";
                this.props.listener ? this.props.listener(data) : null;
            }else if(this.isPickaxeArea(gesture) && this.props.status!=="pickaxe"){
                data.action="play";
                this.props.listener ? this.props.listener(data) : null;
            }
            
            
            Animated.event([
                null, { dx: this.state.pan.x, dy: this.state.pan.y }
            ]);
            this.setState({isHide: false});
        } else {
            Animated.spring(this.state.pan, {
              toValue: { x: 0, y: 0 }
            }).start();
        }
      }
    });
  }
  
  isDropArea(gesture) {
    return gesture.moveX >= 80 && gesture.moveY >= 550;
  }
  
  isPickaxeArea(gesture) {
    return gesture.moveX >= 180 && gesture.moveX <= 380 && 
            gesture.moveY >= 240 && gesture.moveY <= 400;
  }
  
  _cardValue(style){
      return (
        <View style={style}>
            <Text style={CardStyles(this.props).textCard}>{getSymbole(this.props)}</Text>
            <Emoji name={this.props.color} style={{fontSize: 18}} />
        </View>
      );
  }
  
  render() {
    var styles = CardStyles(this.props);
    const panStyle = {
      transform: this.state.pan.getTranslateTransform(),
      zIndex: this.state.zIndex,
      left: this.props.leftValue ? this.props.leftValue : 0,
      top: this.props.topValue ? this.props.topValue : 0
    }
    return (
        <Animated.View 
            {...this.panResponder.panHandlers}
            style={[panStyle,styles.container]}>
            {this.state.isHide ?
            <View style={styles.containerCardHide} /> :
            <View style={styles.containerCard}>
                {this._cardValue(styles.containerValueTopLeft)}
                {this._cardValue(styles.containerValueTopRight)}
                <Emoji name={this.props.color} style={styles.cardColor}/>
                {this._cardValue(styles.containerValueBottomLeft)}
                {this._cardValue(styles.containerValueBottomRight)}
            </View>}
        </Animated.View>

    );
  }
} 