import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Emoji from 'react-native-emoji';

export default class Player extends React.Component {
  
  constructor (props) {
    super(props)
  }
 
  render() {
      return (
        <View style={styles.container}>
           <Emoji name={this.props.emoji ? this.props.emoji : "grin"} style={{fontSize: 35}} />
           <View style={styles.containerPlayer}>
                <Text>{this.props.name}</Text>
                <View style={styles.containerPlayerCards}>
                    <Text>{this.props.cards}</Text>
                    <Emoji name="black_joker" style={{fontSize: 18}} />
                </View>
           </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flexDirection: "row",
   margin: 5
  },
  containerPlayer:{
      
  },
  containerPlayerCards:{
    flexDirection: "row"
  }
});