import { StyleSheet } from 'react-native';
import { getRgbColor} from "../helpers/CardsHelper";

export default cardStyles = (card) => { return StyleSheet.create({
    container: {
      position: "absolute",
      width: 100,
      height: 170,
      backgroundColor: '#fff',
      borderColor: "#000",
      borderWidth: 1,
      borderRadius: 5
    },
    containerCard: {
      padding: 2,
      margin: 2,
      width: 94,
      height: 164,
      borderWidth: 1,
      borderColor: getRgbColor(card)
    },
    containerCardHide :{
        backgroundColor: "#6495ED",
        padding: 2,
        margin: 2,
        width: 94,
        height: 164
    },
    textCard:{
      color: getRgbColor(card),
      fontSize: 18,
      paddingLeft: card.value!==10 ? 6: 0
    },
    cardColor:{
      fontSize: 36,
      top: 50,
      left: 22
    },
    containerValueTopLeft:{
      position: "absolute",
      margin: 2
    },
    containerValueTopRight:{
      position: "absolute",
      margin: 2,
      left: 65
    },
    containerValueBottomLeft:{
      position: "absolute",
      margin: 2,
      top: 110,
      transform: [{ rotate: '180deg' }]
    },
    containerValueBottomRight:{
      position: "absolute",
      margin: 2,
      left: 65,
      top: 110,
      transform: [{ rotate: '180deg' }]
    }
  });
}
 
 