import React from 'react';
import { View, TextInput, Text, FlatList, StyleSheet, Alert } from 'react-native';
import { CustomPicker } from 'react-native-custom-picker'
import Emoji from 'react-native-emoji';
import {retrieveUserSetting, setUsername, setEmoji}  from '../services/asyncstorage';

export default class PlayerSettings extends React.Component {
  
  constructor (props) {
    super(props)
    this.state = {text: '', emoji: 'grin'};
    retrieveUserSetting().then(settings=>{
        this.setState({emoji: settings.emoji, text: settings.username}); 
    })
  }
  
  renderField(settings) {
    const { selectedItem, defaultText, getLabel, clear } = settings
    return (
      <View style={styles.container}>
        <View>
          {!selectedItem && <Text style={[styles.text, { color: 'grey' }]}>{defaultText}</Text>}
          {selectedItem && (
            <View style={styles.innerContainer}>
              <Emoji name={selectedItem} style={{fontSize: 35}} />
            </View>
          )}
        </View>
      </View>
    )
  }
  
  renderOption(settings) {
    const { item, getLabel } = settings
    return (
      <View style={styles.optionContainer}>
        <Emoji name={getLabel(item)} style={{fontSize: 35}} />
      </View>
    )
  }
  
  onEmojiChange(value){
      this.setState({emoji: value});
      this.savePlayerSettings(this.state.text, value);
      setEmoji(value);
  }
  
  onUserNameChange(value){
      this.setState({text: value});
      this.savePlayerSettings(value, this.state.emoji);
      setUsername(value);
  }
  
  savePlayerSettings(username, emoji){
      var message = JSON.stringify({
            action: "rename",
            data: JSON.stringify({username: username, emoji: emoji})
      });
      if(this.props.websocket)
        this.props.websocket.send(message);
  }

  render() {
      const options=['grin', 'boy', 'girl', 'princess', 'robot_face', 'smiling_imp', 
                      'sunglasses', 'confounded', 'stuck_out_tongue_winking_eye',
                      'rage', 'cry', 'thinking_face', 'hankey' ];
      return (
        <View style={styles.container}>
           <CustomPicker
            modalStyle={{width: 100}}
            modalAnimationType="slide"
            options={options}
            fieldTemplate={this.renderField}
            optionTemplate={this.renderOption}
            onValueChange={this.onEmojiChange.bind(this)}
            value={this.state.emoji}
            defaultValue='grin'
           />
           <TextInput 
                style={styles.textInput}
                placeholder="Pseudonyme"
                onChangeText={ this.onUserNameChange.bind(this) }
                value={this.state.text}
                onEndEditing={this.inEmojiChange} />
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    margin: 5
  },
  textInput:{
    marginLeft: 20
  },
  optionContainer:{
    alignItems: 'center',
    justifyContent: "center",
  }
});