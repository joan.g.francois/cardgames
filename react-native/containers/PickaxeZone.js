import React from 'react';
import { View } from 'react-native';
import Card from "../components/Card"

export default function PickaxeZone(props){
    return (
        <View style={props.style}>
            {props.pickaxeCards.map((item,key)=>{
                var leftValue = 72 + (getRandomInt(0, 35) * (key%2===0 ? 1 : -1));
                var topValue = 12  + (getRandomInt(0, 15) * (key%2===0 ? 1 : -1));
                return  <Card key={key}
                              index={key}
                              color={item.color} 
                              value={item.value} 
                              isHide={false} 
                              leftValue={leftValue}
                              topValue={topValue}
                              status="pickaxe"
                              listener={props.listener} />;
            })}
        </View>);
}

const getRandomInt= (min, max)=> {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
};

