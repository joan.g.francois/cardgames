import React, {Component} from 'react';
import { translate } from "../local/local";
import {Modal, Text, TextInput, TouchableHighlight, Button, View, Platform, StyleSheet} from 'react-native';
import { Slider } from 'react-native-elements';

export default class ModalCreateGame extends Component {
  constructor (props) {
    super(props)
    this.state = {
        modalVisible: props.visible,
        text: '',
        styleText: styles.textInput,
        nbrPlayers: 5
    };
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  
  createGame(){
    if(this.state.text!==''){
        const {navigate} = this.props.navigation;
        this.setState({modalVisible:false});
        var message = JSON.stringify({
                action: "creategame",
                data: JSON.stringify({name: this.state.text, nbrPlayer: this.state.nbrPlayers})
          });                              
          this.props.websocket.send(message);
        navigate('Game', {gamename: this.state.text, 
                          websocket:this.props.websocket});
    }else{
        this.setState({styleText:styles.textInputError});
    }
  }

  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setState({modalVisible: false});
          }}>
          <View style={styles.container}>
            <View style={styles.tabBarInfoContainer}>
                 <Text style={{fontSize:24}}>{translate("CREATEGAME")}</Text>
            </View>
            <View style={styles.containerForm}>
                <TextInput 
                    style={this.state.styleText}
                    placeholder={translate("ENTERNAMEGAME")}
                    onChangeText={(text) => {
                        this.setState({text: text, styleText: styles.textInput});
                    }}
                    value={this.state.text}/>
                    
                <Text style={styles.sliderPlayersText}>{translate("MAXPLAYER")} : {this.state.nbrPlayers}</Text>
                <Slider
                    value={this.state.nbrPlayers}
                    step={1}
                    minimumValue={2}
                    maximumValue={9}
                    thumbTintColor={'#2f95dc'}
                    style={styles.sliderPlayers}
                    onValueChange={value => this.setState({ nbrPlayers: value })}
                />
            </View>
            <View style={styles.tabBarInfoContainer}>
                  <Button title={translate("PLAY")}
                          onPress={this.createGame.bind(this)}></Button>
            </View>
          </View>
        </Modal>
        <Button
            title={translate("CREATEGAME")}
            onPress={() => {
                this.setModalVisible(true);
        }}></Button>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    margin: 20,
    marginTop: 170
  },
  containerForm:{
      flex: 9,
      backgroundColor: '#e1e8ee',
  },
  textInput:{
       margin:20,
       padding: 10
  },
  textInputError:{
       margin:20,
       padding: 10,
       borderColor: 'red',
       borderWidth: 1
  },
  sliderPlayersText:{
        marginLeft:30,
  },
  sliderPlayers:{
      marginLeft:30,
      width: 200
  },
  tabBarInfoContainer: {
     flex: 1,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
});