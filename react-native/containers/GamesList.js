import React from 'react';
import { View, Text, FlatList, TouchableWithoutFeedback, StyleSheet, Alert } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { translate } from "../local/local";

export default class GamesList extends React.Component {
  
  constructor (props) {
    super(props)
    this.state = {
        loading: false,      
        data: props.data,
        search: '',
        error: null,
        listSize: props.data.length
    };
    this.arrayholder = props.data;
  }
  
  shouldComponentUpdate(nextProps, nextState){
     if(this.state.listSize!==nextProps.data.length){
         nextState.data = nextProps.data;
         nextState.listSize = nextProps.data.length;
         this.arrayholder = nextProps.data;
         return true;
     }
     return this.state.search!==nextState.search;
  }
  
  searchFilterFunction(text){
      const newData = this.arrayholder.filter(item => {      
         const itemData = `${item.gamename.toUpperCase()}`;
         const textData = text.toUpperCase();
         return itemData.indexOf(textData) > -1;    
      });    
      this.setState({ data: newData, search: text });
  }
  
  selectGame(item){
      const {navigate} = this.props.navigation;
      Alert.alert(  translate("JOINGAME"),
                    translate("CONFIRMJOINGAME")+'\n \"'+item.gamename+"\"",
                    [
                      {
                        text: translate("CANCEL"),
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                      },
                      {text: translate("CONFIRM"), onPress: () => {
                            console.log(item.gameId);
                            this.props.websocket.send(JSON.stringify({
                                action: "joingame",
                                data: JSON.stringify({gameId: item.gameId}) 
                            }));
                            navigate('Game', {gameId:item.gameId,
                                              gamename: item.gamename, 
                                              websocket:this.props.websocket});
                            
                        }
                    },
                    ],
                    {cancelable: false});
  }
  
  itemList = (props) =>{
        return (
            <TouchableWithoutFeedback onPress={ () => this.selectGame(props.item)}>
                <View style={styles.itemGame}>
                    <Text style={{flex:7}}>{props.item.gamename}</Text>         
                    <Text style={{flex:3}}>{translate("MAXPLAYER")} : {props.item.maxPlayers}</Text>
                </View>
            </TouchableWithoutFeedback>  
        );
      
  }

  render() {
      let ItemGameData = this.itemList;
      return (
        <View style={styles.container}>
            <SearchBar
                 placeholder={translate("SEARCHGAME")}        
                 lightTheme
                 value={this.state.search}
                 containerStyle={{width: 320}}
                 onChangeText={text => this.searchFilterFunction(text)}
                 autoCorrect={false}       
            />
            <FlatList          
                data={this.state.data}          
                renderItem={({ item }) => ( 
                    <ItemGameData item={item} />
                 )}          
                 keyExtractor={item => item.gameId}                          
             />            
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1  
  },
  itemGame:{
      padding: 10,
      backgroundColor: '#e1e8ee',
      color: '#5e6977',
      borderColor:'#fff',
      borderWidth: 1,
      flexDirection: 'row'
  }
});