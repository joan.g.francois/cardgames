import React from 'react';
import { View } from 'react-native';
import Card from "../components/Card"


export default function PlayerHand(props){
    var count=0;
    var topHand=0
    return (
        <View style={props.style}>
            {props.playerHand.map((item,key)=>{
                if(count===10){
                    count=0;
                    topHand= 40;
                }
                return  <Card key={key}
                              index={key}
                              color={item.color} 
                              value={item.value} 
                              isHide={false}
                              topValue={topHand}
                              leftValue={count++*30}
                              status="playerHand"
                              listener={props.listener}/>;
            })}
        </View>);
}

