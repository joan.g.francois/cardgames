import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Card from "../components/Card"
import Layout from "../constants/Layout"

export default class Deck extends React.Component {
  
  constructor (props) {
    super(props)
    this.state= { cards: this.props.cards};
  }
  
  shouldComponentUpdate(nextProps, nextState){
      nextState.cards = nextProps.cards;
      return true;
  }
  
  render() {
      return (
        <View style={styles.container}>
            {this.state.cards.map((item, key)=>{
                return <Card key={key}
                             index={key}
                             color={item.color} 
                             value={item.value} 
                             isHide={true}
                             status="deck"
                             listener={this.props.listener}
                       />
            })}
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin:12,
    marginTop:30
  }
});