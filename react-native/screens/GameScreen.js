import React from 'react';
import { View, StyleSheet, Text, Button, Alert } from 'react-native';
import Deck from "../containers/Deck";
import Card from "../components/Card"
import Player from  "../components/Player"
import {initCards} from "../helpers/CardsHelper"
import {retrieveUserSetting}  from '../services/asyncstorage';
import RestartButton from "../components/RestartButton";
import PlayerHand from "../containers/PlayerHand";
import PickaxeZone from "../containers/PickaxeZone";
import { translate } from "../local/local";

export default class GameScreen extends React.Component {
  
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('gamename', 'NO-ID'),
    };
  };
  
  constructor (props) {
    super(props)
    const { navigation } = this.props;
    //console.log(navigation.getParam('data', []));
    this.state= {
        myId:null,
        gameId: navigation.getParam('gamename', 'NO-ID'),
        players: [],
        cards: [],
        nbrCards:0,
        playerHand: [],
        pickaxeCards: []
    };
    this.websocket= navigation.getParam('websocket', false)
    this.websocket.onmessage = this._onMessageReceive.bind(this);
  }
  
    _onMessageReceive = (e) => {
        var message= JSON.parse(e.data);
        if(message.action==='myid'){
            this.setState({myId: message.data})
        }else if(message.action==='game'){
            if(this.state.players.length===0){
                retrieveUserSetting().then(settings=>{
                    this.state.players.push({
                        connectionId: this.state.myId,
                        username: settings.username,
                        emoji: settings.emoji,
                        nbrCards: 0
                    });
                    this.setState({players: this.state.players});
                 });
            }
            this.setState({gameId: message.data.gameId,
                            cards:  message.data.body.deck,
                            pickaxeCards:  message.data.body.pickaxe
                           });
        }else if(message.action==='info'){
            this.setState({gameId: message.data.game.gameId,
                           cards:  message.data.game.body.deck,
                           pickaxeCards:  message.data.game.body.pickaxe,
                           players: message.data.players,
                           myId: message.data.myid
                          });
            if(message.data.status==='noplace'){
                const { navigate } = this.props.navigation;
                Alert.alert(message.data.game.gamename, translate("GAMEISFULL"));
                navigate('Home');
            }
        }else if(message.action==='joingame'){
            if(!this._isInGame(message.data)){
                var players= this.state.players;
                players.push(message.data);
                this.setState({players: players});
            }
        }else if(message.action==='drawCard'){
            if(message.data.playerId !== this.state.myId){
                if(message.data.cardFrom==="deck"){
                    delete this.state.cards[message.data.cardIndex];
                }else{
                    delete this.state.pickaxeCards[message.data.cardIndex];
                }
            }
            this._updatePlayer(message.data.playerId, message.data.nbrCards);
        }else if(message.action==='playCard'){
           if(message.data.playerId != this.state.myId){
                if(message.data.cardFrom==="deck"){
                    delete this.state.cards[message.data.cardIndex];
                }
                this.state.pickaxeCards.push(message.data.card);
            }
            this._updatePlayer(message.data.playerId, message.data.nbrCards);
        }else if(message.action==='startGame'){
            for(var i in this.state.players){
                this.state.players[i].nbrCards= message.data.nbrCards;
                for(var n=0; n< message.data.nbrCards; n++ ){
                    var card = this.state.cards.pop();
                    if(i===message.data.rang){
                        this.state.playerHand.push(card);
                    }
                }
            }
            this.setState({players: this.state.players, playerHand: this.state.playerHand, nbrCards: message.data.nbrCards});
        }else if(message.action==="quiteGame"){
            var players = this._deletePlayer(message.data.playerId);
            this.setState({players: players});
        }else if(message.action==="infoDeck"){
            this.setState({cards:  message.data.deck,
                           pickaxeCards:  message.data.pickaxe,
                          });
        }else  if(message.action==="restartGame"){
            for(var i in this.state.players){
                this.state.players[i].nbrCards = 0;
            }
            this.setState({cards:  message.data.deck,
                           pickaxeCards:  message.data.pickaxe,
                           players: this.state.players,
                           playerHand: []
                          });
        } 
    }
    
    _isInGame(player){
        for(var i in this.state.players){
            if(this.state.players[i].connectionId === player.connectionId){
                return true;
            }
        };
        return false;
    }
    
    _deletePlayer(playerId){
        var players= [];
        for(var i in this.state.players){
            if(this.state.players[i].connectionId !== playerId){
                players.push(this.state.players[i]);
            }
        };
        return players;
    }
    
    _updatePlayer(connectionId, nbrCards){
        for(var i in this.state.players){
            if(this.state.players[i].connectionId === connectionId){
                this.state.players[i].nbrCards = nbrCards;
            }
        };
        this.setState({players: this.state.players, });
    }
  
  _listener(data){
      var cardPlay;
      if(data.action==="play"){
          if(data.status==="playerHand"){
            this.state.nbrCards--;
            cardPlay= this.state.playerHand[data.index];
            this.state.pickaxeCards.push(cardPlay);
            delete this.state.playerHand[data.index];
          }else{
            cardPlay= this.state.cards[data.index];
            this.state.pickaxeCards.push(cardPlay)
            delete this.state.cards[data.index];
          }
          this._sendData("playcard", this.state.nbrCards, data.status, cardPlay, data.index);
      }else if(data.action==="draw"){
          this.state.nbrCards++;
          if(data.status==="deck"){
            cardPlay= this.state.cards[data.index];
            this.state.playerHand.push(cardPlay)
            delete this.state.cards[data.index];
          }else{
            cardPlay= this.state.pickaxeCards[data.index];
            this.state.playerHand.push(cardPlay)
            delete this.state.pickaxeCards[data.index];
          }
          this._sendData("drawcard", this.state.nbrCards, data.status, cardPlay, data.index);
      }
      this.setState({cards: this.state.cards,
                     playerHand: this.state.playerHand,
                     pickaxeCards: this.state.pickaxeCards});
  }
  
  _sendData(action, nbrCards, cardFrom, card, index){
        this.websocket.send(JSON.stringify({
            action: action,
            data: JSON.stringify({
                gameId: this.state.gameId,
                nbrCards: nbrCards,
                cardFrom: cardFrom,
                card: card,
                cardIndex:index
            })}) 
        );
  }
  
  _distribuer(e){
      for(var n in this.state.players){   
        this.websocket.send(JSON.stringify({
            action: "startgame",
            data: JSON.stringify({
                gameId: this.state.gameId,
                playerId: this.state.players[n].connectionId,
                nbrCards: 7,
                nbrPlayers: this.state.players.length,
                rang: n})
            }) 
        );
      }
  }
  
  _gameStarted(){
        var cardsPlayers= this.state.playerHand.length;
        for(var i in this.state.players){
           cardsPlayers += this.state.players[i].nbrCards
        }
        return !(cardsPlayers===0 && this.state.pickaxeCards.length===0);
  }
  
  
  componentWillUnmount(){
    console.log("quit game");
    this.websocket.send(JSON.stringify({action:"quitegame",
                                        data: JSON.stringify({gameId: this.state.gameId})}));
    this.websocket.close();
  }
  
  render() {
    return (
        <View style={styles.container}>
            <RestartButton 
                style={styles.buttonRestart}
                gameId={this.state.gameId}
                websocket={this.websocket}/>
            <View style={styles.containerPlayers}>
                {this.state.players.map((item,key)=>{
                    return  <Player key={key} name={item.username} cards={item.nbrCards} emoji={item.emoji}/>;
                })}
            </View>
            <View style={styles.containerGame}>
                <View style={styles.containerDeck}>
                    <Deck cards={this.state.cards} listener={this._listener.bind(this)}/>
                </View>
                <View style={styles.containerPickaxe}>
                    { !this._gameStarted() ?
                        <Button
                            onPress={this._distribuer.bind(this)}
                            title={translate("STARTGAME")}
                        /> : 
                        <PickaxeZone style={styles.pickaxeZone}
                            pickaxeCards={this.state.pickaxeCards}
                            listener={this._listener.bind(this)}
                        />
                    }
                </View>
            </View>
            <View style={styles.playerHand}>
                <PlayerHand style={styles.playerHandZone}
                    playerHand={this.state.playerHand}
                    listener={this._listener.bind(this)}
                />
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
  },
  buttonRestart:{
      position: "absolute",
      right: 20,
      top: 10
  },
  containerPlayers:{
    flex: 1,
    flexDirection: "row",
    flexWrap: 'wrap',
  },
  containerGame:{
    flex: 2,
    flexDirection: "row",
  },
  containerDeck:{
    flex: 3,
  },
  containerPickaxe:{
    flex : 7,
    zIndex: -10,
    justifyContent: 'center',
    alignItems: 'center' 
  },
  pickaxeZone: {
      flex: 1,
      margin: 20,
      backgroundColor: "#fff",
      borderColor: "#000",
      borderWidth: 1,
      borderStyle: "dashed",
      borderRadius: 5,
      alignSelf: 'stretch' 
  },
  playerHand : {
      flex: 2
  },
  playerHandZone:{
    flex: 1,
    margin: 20,
    borderWidth: 1,
    borderStyle: "dashed",
    borderRadius: 5
  }
});
