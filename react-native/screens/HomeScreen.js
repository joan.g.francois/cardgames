import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, 
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { translate } from "../local/local";
import PlayerSettings from "../containers/PlayerSettings";
import GamesList from "../containers/GamesList";
import ModalCreateGame from "../containers/ModalCreateGame";
import {retrieveUserSetting}  from '../services/asyncstorage';
import WebService from '../constants/WebService';

export default class HomeScreen extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
        createVisible: false,
        listgames: [],
    };
    this.ws = null;
  }
  
  _initConnection(){
    console.log("connection");
    this.ws = new WebSocket(WebService.ServiceEndpoint);
    this.ws.onmessage = this._onMessageReceive.bind(this);
    var self= this;
    this.ws.onopen = ()=>{
        console.log("getgames");
        self.ws.send(JSON.stringify({action:"getgames"}));
        retrieveUserSetting().then(settings=>{
            self.savePlayerSettings(settings.username, settings.emoji);
        });
    };
  }
  
  componentWillUnmount(){
    console.log("quit app");
    this.ws.close();
  }
  
  savePlayerSettings(username, emoji){
      var message = JSON.stringify({
            action: "rename",
            data: JSON.stringify({username: username, emoji: emoji})
      });
      if(this.ws)
        this.ws.send(message);
  }
  
  _onMessageReceive = (e) => {
      var message= JSON.parse(e.data);
      if(message.action==='games'){
          this.setState({listgames: message.data})
      }else if(message.action==='game'){
          var games= this.state.listgames;
          games.push(message.data);
          console.log("update listgame (add: "+message.data.gamename+")");
          this.setState({listgames: games})
      }else if(message.action==='refreshlistgame'){
          this.ws.send(JSON.stringify({action:"getgames"}));
      }
  }
  
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
        <NavigationEvents
            onDidFocus={payload => this._initConnection()}
        />
        <View
          style={styles.container}
          contentContainerStyle={styles.contentContainer}>
          <View style={styles.titleContainer}>
            <Text style={{fontSize:28}}>{translate("TITLE")}</Text>
          </View>
          <View style={styles.settingsPlayer}>
            <PlayerSettings websocket={this.ws} />
          </View>
          <View style={styles.gameListContainer}>
            <GamesList navigation={this.props.navigation}
                       websocket={this.ws}
                       data={this.state.listgames}/>
          </View>
          
        </View>
        <View style={styles.tabBarInfoContainer}>
            <ModalCreateGame visible={this.state.createVisible}
                             navigation={this.props.navigation}
                             websocket={this.ws}/>
        </View>
      </View>
    );
  }
}

HomeScreen.navigationOptions = {
  header: null
};

function DevelopmentModeNotice() {
  if (__DEV__) {
    return (
      <Text style={styles.developmentModeText}>
        Development mode is enabled: your app will be slower but you can use
        useful development tools.
      </Text>
    );
  } else {
    return (
      <Text style={styles.developmentModeText}>
        You are not in development mode: your app will run at full speed.
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 30,
  },
  titleContainer: {
    alignItems: 'center',
    justifyContent: "center",
    marginTop: 25,
    flex: 1,
  },
  settingsPlayer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: "center",
  },
  gameListContainer:{
    flex: 7,
    alignItems: 'center',
    justifyContent: "center",
    marginBottom: 90
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
