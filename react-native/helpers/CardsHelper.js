
export const colors = ["hearts", "diamonds", "clubs", "spades"];

export const initCards = () => {
    var cards =[];
    for(var i in colors){
        for(var j=2; j<=14; j++){
            cards.push({color: colors[i], value: j});
        }
    }
    cards.sort(() => Math.random() - 0.5);
    return  cards;
};

export const getRgbColor = (card)=>{
    switch(card.color){
        case "hearts" :
        case "diamonds" : 
            return "#F00";
        default : return "#000";
    }
};

export const getSymbole = (card)=>{
    switch(card.value){
        case 11 : return "V";
        case 12 : return "D";
        case 13 : return "R";
        case 14 : return "A";
        default : return card.value;
    }
};
